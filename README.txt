# Bootstrap grunt

This is a documenation for this sub-theme using grunt + bower.
Bootstrap grunt is a subtheme to the drupal bootstrap project 
(https://drupal.org/project/bootstrap) and putting grunt and bower in good use.

The goal of this sub-theme to ease package management for theme developers and 
give them the control to choose which processor port of twitter bootstrap 
they would want to use.

*Examples*:

* Bootstrap stylus: https://github.com/D1plo1d/bootstrap-stylus
* Bootstrap sass: https://github.com/thomas-mcdonald/bootstrap-sass

## The use of Grunt:

* Local pre-processor for LESS, SASS or stylus utilising node.
* Livereload support ( tested in a local installation ).
* Giving you control over which instance of bootstrap you would want to
  install. Whether it be LESS, SASS och stylus. All this using node as your
  local processor.

## The use of bower:

* For you to configure necessary dependencies and their versions that are
  necessary for your theme.

## Future development:

* Nothing will happen until you actually test this and give me your feedback!
* I'm open to your ideas on what you would want in this sub-theme, since you
  are the main user of this theme.

# Installation:

* Download bootstrap! (https://drupal.org/project/bootstrap)
* npm install
* bower install

the `bower install` command has a requirement of jQuery, so it comes along
when you run this. Since (drupal) bootstrap is using drupals internal jQuery version, 
feel free to delete the jQuery folder.

Now your theme is ready almost ready to be used.

# Configure First! (Always)

## Making use of livereload ( local installation ):

* Add the following script in your <head> 
    `<script src="http://localhost:35729/livereload.js"></script>`
* Uncomment the following code in the `Gruntfile.js`

```javascript
  // ------------------------
  // , options : {
  //   livereload : true
  // }
  // ------------------------
```

# Original documenation:
To help consolidate and remove possible futureinconsistencies, the
documentation for this sub-theme starter kit has been moved to:
http://drupal.org/node/1978010.
