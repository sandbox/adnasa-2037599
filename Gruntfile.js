// @TODO: Target components/bootstrap/js to be compiled utilising uglifyJS
// or something equivalent. When that is done, we are ready to cleanup the
// .info file.

// @TODO: Add support for stylus and sass instances of bootstrap.

// Documentation: http://gruntjs.com/configuring-tasks
module.exports = function(grunt) {
  grunt.initConfig({
    less: {
      // Development base task.
      development : {
        options : {
          compress: true,
          paths: ['components/bootstrap/less']
        },
        files :{
          // string|Array type of structure.
          'components/css/style.css' : 'components/less/style.less',
        },
      }
      // @TODO: Add production less task option.
    },
    watch : {
      styles : {
        files : ['components/less/*.less'],
        tasks : ['less'],
      }
      // Want to see live reload in action in your local installation?
      // Uncomment the options object and add the following script in <head>:
      //   <script src="http://localhost:35729/livereload.js"></script>

      // ------------------------ 
      // , options : {
      //   livereload : true
      // }
      // ------------------------
    }
    // Other tasks follows here...
  });

  // Load plugins.
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // register default tasks.
  grunt.registerTask('default', ['watch']);
}
